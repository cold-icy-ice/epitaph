# Epitaph

Epitaph is a useless picture manipulation software.

Find us on https://se.ifmo.ru/~s225088/.

Made with :heart:.

## License

© 2017 Egor Dubenetskiy and Ilya Shulgin

Epitaph is released under the MIT License. :thinking:
