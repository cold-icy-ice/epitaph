<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Лабораторная работа №&nbsp;1</title>
  <link rel="stylesheet" href="css/check.css">
  <link rel="shortcut icon" href="img/icon.png">
</head>
<body>
<table class="container">
    <?php include 'header.php' ?>
  <tr id="fill">
    <td class="content" colspan="7">
        <?php

        include_once 'lib/Area.php';
        include_once 'lib/Point.php';
        include_once 'lib/InvalidParametersException.php';
        include_once 'lib/Task.php';
        include_once 'lib/TaskFormatter.php';

        const STORED_RECENT_TASKS = 3;

        $formatter = new \epitaph\TaskFormatter(fopen("php://output", 'w'));
        $task = new \epitaph\Task($_GET['x'], $_GET['y'], $_GET['r']);

        session_start();
        $_SESSION['tasks'][] = $task;
        if (count($_SESSION['tasks']) > STORED_RECENT_TASKS) {
            $_SESSION['tasks'] = array_slice($_SESSION['tasks'], -STORED_RECENT_TASKS);
        }

        ?>
      <table class="result">
        <thead>
        <tr>
          <th class="short">x</th>
          <th class="short">y</th>
          <th class="short">r</th>
          <th>Результат</th>
          <th>Время</th>
          <th>Потрачено</th>
        </tr>
        </thead>
          <?php
          foreach ($_SESSION['tasks'] as $task) {
              $formatter->format($task);
          }
          ?>
      </table>
      <form action="index.php">
        <button id="back" type="submit" class="crimson back">Назад</button>
      </form>
    </td>
    <td class="sidebar" colspan="5">
        <?php include 'area.php'; ?>
    </td>
  </tr>
    <?php include 'footer.php'; ?>
</table>
</body>
</html>
