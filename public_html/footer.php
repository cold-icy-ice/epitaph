<tr id="footer">
  <td class="contacts" colspan="3">
    <span class="helper"></span>
    <a id="repository" href="https://gitlab.com/edubenetskiy/epitaph" target="_blank">
      <img src="./img/gitlab.png" style="vertical-align: middle; max-height: 15px;">
      <span>edubenetskiy/epitaph</span>
    </a>
  </td>
  <td class="copy" colspan="6">
    &copy; Егор Дубенецкий и Илья Шульгин, 20<span style="color: #eb3b67;">!</span>7
  </td>
  <td class="institution" colspan="3">
    <img src="./img/slogan.png" alt="IT's MOre than a university">
  </td>
</tr>
