<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Лабораторная работа №&nbsp;1</title>
  <link rel="stylesheet" href="css/index.css">
  <script type="text/javascript" src="js/form.js"></script>
  <link rel="shortcut icon" href="img/icon.png">
</head>
<body>
<table class="container">
    <?php include 'header.php'; ?>
  <tr id="fill">
    <td class="content" colspan="7">
      <form name="parameters" action="check.php" method="GET" onsubmit="return validateForm(this)">
        <div id="r-group">
          <span>
            R =
          </span>
          <label>
            <input type="radio" name="r" value="1" checked>1
            <div class="check">
              <div class="inside"></div>
            </div>
          </label>
          <label>
            <input type="radio" name="r" value="2"> 2
            <div class="check">
              <div class="inside"></div>
            </div>
          </label>
          <label>
            <input type="radio" name="r" value="3"> 3
            <div class="check">
              <div class="inside"></div>
            </div>
          </label>
          <label>
            <input type="radio" name="r" value="4"> 4
            <div class="check">
              <div class="inside"></div>
            </div>
          </label>
          <label>
            <input type="radio" name="r" value="5"> 5
            <div class="check">
              <div class="inside"></div>
            </div>
          </label>
        </div>
        <div id="x-group">
          <label for="x">
            X =
            <input type="number" name="x" hidden value="0"/>
          </label>
          <button type="button" class="x-button" value="-4">-4</button>
          <button type="button" class="x-button" value="-3">-3</button>
          <button type="button" class="x-button" value="-2">-2</button>
          <button type="button" class="x-button" value="-1">-1</button>
          <button type="button" class="x-button" value="0">0</button>
          <button type="button" class="x-button" value="1">1</button>
          <button type="button" class="x-button" value="2">2</button>
          <button type="button" class="x-button" value="3">3</button>
          <button type="button" class="x-button" value="4">4</button>
        </div>
        <div id="y-group">
          <label for="y">
            Y =
          </label>
          <input type="text" name="y" id="y" placeholder="(-5, +5)">
        </div>
        <button id="submit" type="submit" class="crimson submit">Проверить!</button>
      </form>
    </td>
    <td class="sidebar" colspan="5">
        <?php include 'area.php'; ?>
    </td>
  </tr>
    <?php include 'footer.php' ?>
</table>
</body>
</html>
