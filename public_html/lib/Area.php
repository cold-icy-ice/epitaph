<?php

namespace epitaph;

/**
 * Class which represents the area consisting of a rectangle, a triangle and a circle quadrant.
 * @package epitaph
 */
class Area
{
    private $radius;

    function __construct($radius)
    {
        $this->setRadius($radius);
    }

    private function setRadius($radius)
    {
        $this->radius = preg_match('/^[+-]?\d+$/', $radius) ? intval($radius) : null;
        if (is_null($radius) || $this->radius < 1 || $this->radius > 5) {
            throw new InvalidParameterException("Неверно указан параметр ‘r’. Пожалуйста, укажите целое значение от 1 до 5.<br>\n");
        }
    }

    /**
     * @param $point Point
     * @return bool
     */
    public function contains($point)
    {
        $x = $point->getX();
        $y = $point->getY();

        $inRectangle =
            (-$this->radius / 2 <= $x && $x <= 0) &&
            (0 <= $y && $y <= $this->radius);

        $inTriangle =
            ($x + $y * 2 >= -$this->radius) &&
            ($x <= 0 && $y <= 0);

        $inCircleQuadrant =
            ($x >= 0 && $y <= 0) &&
            ($x * $x + $y * $y <= $this->radius * $this->radius);

        $result = $inRectangle || $inTriangle || $inCircleQuadrant;
        return $result;
    }

    public function getRadius()
    {
        return $this->radius;
    }
}
