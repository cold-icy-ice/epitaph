<?php

namespace epitaph;
include_once 'InvalidParametersException.php';

class InvalidParameterException extends InvalidParametersException
{
    private $htmlMessage;

    public function __construct($htmlMessage)
    {
        parent::__construct($htmlMessage);
        if (is_string($htmlMessage)) {
            $this->htmlMessage = $htmlMessage;
        } else {
            throw new \Exception("htmlMessage must be a string");
        }
    }

    function getHtmlMessage()
    {
        return $this->htmlMessage;
    }
}
