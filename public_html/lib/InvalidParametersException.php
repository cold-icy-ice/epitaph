<?php

namespace epitaph;

abstract class InvalidParametersException extends \Exception
{
    abstract function getHtmlMessage();
}
