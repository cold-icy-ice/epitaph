<?php

namespace epitaph;
include_once 'InvalidParameterException.php';
include_once 'InvalidParametersException.php';
include_once 'SeveralInvalidParametersException.php';

/**
 * Class which represents a point lying on a plane.
 * @package epitaph
 */
class Point
{
    const MIN_X = -4;
    const MAX_X = +4;

    const MIN_Y = -5;
    const MAX_Y = +5;

    private $x;
    private $y;

    function __construct($x, $y)
    {
        $exceptions = array();

        try {
            $this->setX($x);
        } catch (InvalidParameterException $ipe) {
            array_push($exceptions, $ipe);
        }

        try {
            $this->setY($y);
        } catch (InvalidParameterException $ipe) {
            array_push($exceptions, $ipe);
        }

        if ($exceptions) {
            throw new SeveralInvalidParametersException($exceptions);
        }
    }

    private function setX($x)
    {
        $this->x = preg_match('/^[+-]?\d+$/', $x) ? intval($x) : null;
        if (is_null($this->x) || $this->x < self::MIN_X || $this->x > self::MAX_X) {
            throw new InvalidParameterException(
                "Неверно указан параметр ‘x’. " .
                "Пожалуйста, укажите целое значение из интервала " .
                "[" . self::MIN_X . ", " . self::MAX_X . "].<br>\n"
            );
        }
    }

    private function setY($y)
    {
        $this->y = (is_numeric($y) ? floatval($y) : null);
        if (is_null($this->y) || $this->y <= self::MIN_Y || $this->y >= self::MAX_Y) {
            throw new InvalidParameterException(
                "Неверно указан параметр ‘y’. " .
                "Пожалуйста, укажите вещественное значение из интервала " .
                "(" . self::MIN_Y . ", " . self::MAX_Y . ").<br>\n"
            );
        }
    }

    public function getX()
    {
        return $this->x;
    }

    public function getY()
    {
        return $this->y;
    }
}
