<?php

namespace epitaph;
include_once 'InvalidParametersException.php';

/**
 * Class SeveralInvalidParametersException
 * @package epitaph
 */
class SeveralInvalidParametersException extends InvalidParametersException
{
    /**
     * @var array
     */
    private $exceptions;

    /**
     * SeveralInvalidParametersException constructor.
     * @param string $exceptions
     * @throws \Exception
     */
    public function __construct($exceptions)
    {
        if (is_array($exceptions)) {
            $this->exceptions = $exceptions;
        } else {
            throw new \Exception("Please pass an array of exceptions");
        }
    }

    public function getHtmlMessage()
    {
        $message = '';
        foreach ($this->exceptions as $exception) {
            $message .= $exception->getHtmlMessage();
        }
        return $message;
    }
}
