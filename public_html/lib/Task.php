<?php

namespace epitaph;
include_once "InvalidParametersException.php";
include_once "Point.php";
include_once "Area.php";

class Task
{
    private $x;
    private $y;
    private $r;

    private $point;
    private $area;

    private $complete = false;
    private $result = null;
    private $dateTime = null;
    private $elapsedTime = null;
    private $exception = null;

    public function __construct($x, $y, $r)
    {
        $this->x = $x;
        $this->y = $y;
        $this->r = $r;
    }

    public function isComplete()
    {
        return $this->complete;
    }

    /**
     * @return Area
     */
    public function getArea()
    {
        $this->processLazily();
        return $this->area;
    }

    /**
     * @return Point
     */
    public function getPoint()
    {
        $this->processLazily();
        return $this->point;
    }

    /**
     * @return bool
     */
    public function getResult()
    {
        $this->processLazily();
        return $this->result;
    }

    /**
     * @return \DateTime
     */
    public function getDateTime()
    {
        $this->processLazily();
        return $this->dateTime;
    }

    /**
     * @return float
     */
    public function getElapsedTime()
    {
        $this->processLazily();
        return $this->elapsedTime;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        $this->processLazily();
        return is_null($this->exception);
    }

    /**
     * @return InvalidParametersException
     */
    public function getException()
    {
        $this->processLazily();
        return $this->exception;
    }

    private function process()
    {
        $startTime = microtime(true);
        $exceptions = array();

        try {
            $this->point = new Point($this->x, $this->y);
        } catch (InvalidParametersException $exception) {
            $exceptions[] = $exception;
        }

        try {
            $this->area = new Area($this->r);
        } catch (InvalidParametersException $exception) {
            $exceptions[] = $exception;
        }

        if ($exceptions) {
            $this->exception = new SeveralInvalidParametersException($exceptions);
        } else {
            $this->result = $this->area->contains($this->point);
        }

        $this->elapsedTime = microtime(true) - $startTime;
        $this->dateTime = new \DateTime();
        $this->complete = true;
    }

    public function processLazily()
    {
        if ($this->isComplete()) return;
        $this->process();
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @return mixed
     */
    public function getR()
    {
        return $this->r;
    }

    /**
     * Measures the time that it takes to execute a given callback.
     * @param $callback \Closure an anonymous function to execute.
     * @return float number of seconds elapsed.
     */
    private function measureTime(\Closure $callback)
    {
        $start_time = microtime(true);
        $callback($this);
        return microtime(true) - $start_time;
    }
}
