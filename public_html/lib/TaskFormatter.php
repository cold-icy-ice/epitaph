<?php

namespace epitaph;
include_once 'Task.php';

class TaskFormatter
{
    const ERROR_MESSAGE_HEADER = "Ошибка валидации!<br>\n";

    private $output;

    function __construct($output = STDOUT)
    {
        $this->output = $output;
    }

    public function getOutput()
    {
        return $this->output;
    }

    private function write($something)
    {
        fputs($this->getOutput(), $something);
    }

    function format(Task $task)
    {
        $task->processLazily();
        $this->write("<tr>\n");
        $this->write("<td class='short'>" . htmlspecialchars($task->getX()) . "</td>\n");
        $this->write("<td class='short'>" . htmlspecialchars($task->getY()) . "</td>\n");
        $this->write("<td class='short'>" . htmlspecialchars($task->getR()) . "</td>\n");
        $this->write("<td>");
        if ($task->isSuccessful()) {
            $this->write($task->getResult()
                ? "Точка входит в область.<br>\n"
                : "Точка находится вне области.<br>\n");
        } else {
            $this->write(self::ERROR_MESSAGE_HEADER);
            $this->write($task->getException()->getHtmlMessage());
        }
        $this->write("</td>\n");
        $this->write("<td>{$task->getDateTime()->format("Y-m-d H:i:s")}</td>\n");
        $this->write("<td>" . number_format($task->getElapsedTime() * 1000, 4) . " мс</td>\n");
        $this->write("</tr>\n");
    }
}
